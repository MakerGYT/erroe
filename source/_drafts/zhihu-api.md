---
title: zhihu-api
date: 2020-02-17 18:30:27
tags:
description: 知乎的api
---
#### 获取用户信息
>https://www.zhihu.com/api/v4/members/makergyt

#### 获取专栏信息
>https://zhuanlan.zhihu.com/api/columns/tensorlayer

#### 获取专栏文章列表
>https://zhuanlan.zhihu.com/api/columns/tensorlayer/articles?limit=10&offset=0
