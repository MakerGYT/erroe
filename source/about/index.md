---
title: About me
date: 2018-12-27 10:17:14
comments: false
---
### 简历
☞ [resume.makergyt.com](https://resume.makergyt.com)
### 最近在听
{% meting "2526283537" "netease" "playlist" "mutex:false" "listmaxheight:340px" "preload:none" "theme:#FC6423"%}
### 最近在读
- 贾平凹《秦腔》

### 更新日志
- 2017.3 使用wordpress,由cloudleft(香港)托管，使用gaoyuting.org访问
  - 2017.9 存在恶意后门，遭遇攻击，主机商关停访问，自行删除后未能克服，故下架
- 2017.11 迁移使用typecho，由京东云托管，使用blog.makergyt.com访问
  - 2017.12 京东云主机到期，暂时下架
- 2018.12 迁移使用hexo ，由github托管，使用makergyt.github.io访问，映射为blog.makergyt.com
  - 2019.1 发现百度不会收录github pages,考虑迁移至国内coding,但样式无法加载，暂时放弃迁移
  - 2019.4 部署至coding pages,使用memakergyt.coding.me访问，映射为blog-cn.makergyt.com，百度收录
  - 2020.1 部署至新coding pages,使用https://y2ukau.coding-pages.com访问，映射为blog.makergyt.com，放弃github pages(所在地区联通无法访问github,且github现在受到美国出口法律约束，前景不明朗),但移动网络无法访问。

### 关于性能
为什么托管在coding上，
1. 没有预算买多余的服务器，以及好的服务器
2. 保证稳定性，避免考虑服务器到期后可能多余的迁移工作
3. 持续集成

☞ [About config of this site](config.html)